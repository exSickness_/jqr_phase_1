#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>

#include "bsfunc.h"
/*
int menu(int testVal)
{
	printf("1.Make a board.\n2. Load board.\n0. Quit.\n");
	char userBuf[20];
	fgets(userBuf,20,stdin);

	int rows = 10, cols = 10;
	char** gameBoard;

	if(userBuf[0] == '1')
	{
		
		gameBoard = makeBoard(&rows,&cols);
	}
	else if(userBuf[0] == '2')
	{
		char* buf = malloc(sizeof(char) *50);

		FILE* fp = openFile(&rows,&cols,buf);
		gameBoard = setBoard(fp,rows,cols,buf);
		
		free(buf);
	}
	else if(userBuf[0] == '0')
	{
		return(0);
	}
	else
	{
		printf("Invalid input.\n");
	}

	printf("rows: %d\ncols: %d\n",rows,cols);

	int loop = 1;
	while(loop)
	{
		printf("1. Write board to file.\n2. Fire shot.\n3. Print Board.\n4. Count ships.\n0. Quit.\n");
		char buf[20];
		fgets(buf,20,stdin);

		if(buf[0] == '1')
		{
			writeToFile(gameBoard,rows,cols);
		}
		else if(buf[0] == '2')
		{
			printf("Enter x coord:\n");
			fgets(buf,20,stdin);
			int x = strtol(buf,NULL,10);

			printf("Enter y coord:\n");
			fgets(buf,20,stdin);
			int y = strtol(buf,NULL,10);

			fireShot(gameBoard,x,y);
		}
		else if(buf[0] == '3')
		{
			printBoard(gameBoard,rows,cols);
		}
		else if(buf[0] == '4')
		{
			int count = countShips(gameBoard,rows,cols);
			printf("Number of ships alive: %d\n",count);
		}
		else if(buf[0] == '0')
		{
			freeBoard(gameBoard,rows);
			loop = 0;
		}
		else
		{
			printf("Invalid input.\n");
		}
	}
	// printf("argc is: %d\nargv[0] is: %s\n",argc,argv[0]);
	printf("TESTVAL>>%d\n",testVal);
	return(0);
}
*/
void printBoard(char* gameBoard[],int rows, int cols)
{
	printf("in print board. rows is: %d    cols is: %d\n",rows,cols);

	for(int i = 0; i < rows; i++)
	{
		printf("%02d ",i);
		for(int p = 0; p < cols; p++)
		{
			printf("%c",gameBoard[i][p]);
		}
		printf("\n");
	}
}

int countShips(char* gameBoard[],int rows, int cols)
{
	int p = 0, s = 0, c = 0, d = 0, b = 0, a = 0;
	for(int i = 0; i < rows; i++)
	{
		for(int q = 0; q < cols; q++)
		{
			if(gameBoard[i][q] == 'p')
			{
				p = 1;
			}
			else if(gameBoard[i][q] == 's')
			{
				s = 1;
			}
			else if(gameBoard[i][q] == 'c')
			{
				c = 1;
			}
			else if(gameBoard[i][q] == 'd')
			{
				d = 1;
			}
			else if(gameBoard[i][q] == 'b')
			{
				b = 1;
			}
			else if(gameBoard[i][q] == 'a')
			{
				a = 1;
			}
		}
	}
	return(p+s+c+d+b+a);
}

int fireShot(char* gameBoard[],int x, int y)
{
	printf("Firing a shot at: %d,%d!\n",x,y);
	if( (gameBoard[y][x] != '*') ) // true if not water
	{
		if( (gameBoard[y][x] == 'x') ) // coord already guessed - hit
		{
			printf("Coordinate already hit!\nMISS!\n");
			return(0);
		}
		if( (gameBoard[y][x] == 'm') ) // coord already guessed - miss
		{
			printf("Coordinate already shot at!\nMISS!\n");
			return(0);
		}
		gameBoard[y][x] = 'x';
		printf("HIT\n");
		return(1);
	}
	gameBoard[y][x] = 'm';
	printf("MISS\n");
	return(0);
}

int writeToFile(char* gameBoard[],int rows,int cols)
{
	FILE* fp = fopen("./testBoard","w+"); // WRITE TO FILE TASK #2
	for(int i = 0; i < rows; i++)
	{
		for(int p = 0; p < cols; p++)
		{
			fprintf(fp,"%c",gameBoard[i][p]);
		}
		fprintf(fp,"\n");
	}
	fclose(fp);

	return(0);
}

int placeShip(int len, char* gameBoard[],int rows, int cols, char ship)
{
	srand(time(NULL));
	int startRow = 0, startCol = 0,yDir,xDir;

	startRow = getRandomNum(rows);
	startCol = getRandomNum(cols);

	xDir = getRandomNum(2);
	yDir = getRandomNum(2);
	if(xDir == 0)
	{
		xDir = 0;
	}
	else
	{
		xDir = 1;
	}

	if(yDir == 0)
	{
		yDir = 0;
	}
	else
	{
		yDir = 1;
	}

	int testRow = startRow;
	int testCol = startCol;
	for(int i = 0; i < len; i++)
	{
		if((testRow >= rows) || (testRow < 0)  || (testCol >= cols) || (testCol < 0))
		{
			return(-1);
		}
		if((xDir == 0) && (yDir == 0))
		{
			return(-1);
		}
		if(gameBoard[testRow][testCol] != '*')
		{
			return(-1);
		}
		testRow += yDir;
		testCol += xDir;
	}

	for(int i = 0; i < len; i++)
	{
		printf("%d,%d\n",startRow, startCol);
		gameBoard[startRow][startCol] = ship;
		startRow += yDir;
		startCol += xDir;
	}

	return(0);
}


char** makeBoard(int rows, int cols)
{
	// rows += getRandomNum(16);
	// cols += getRandomNum(16);

	char **gameBoard;
	gameBoard = malloc( (rows) * sizeof(char*) );

	for(int i = 0; i < rows; i++)
	{
		gameBoard[i] = malloc(sizeof(char) * (cols));
	}

	for(int i = 0; i < rows; i++)
	{
		for(int p = 0; p < cols; p++)
		{
			gameBoard[i][p] = '*';
		}
	}

	printf("\nAircraft\n");
	while(placeShip(5,gameBoard,rows,cols,'a') != 0) ;
	printf("\nBattleship\n");
	while(placeShip(4,gameBoard,rows,cols,'b') != 0) ;
	printf("\nDestroyer\n");
	while(placeShip(3,gameBoard,rows,cols,'d') != 0) ;
	printf("\nCruiser\n");
	while(placeShip(3,gameBoard,rows,cols,'c') != 0) ;
	printf("\nSubmarine\n");
	while(placeShip(2,gameBoard,rows,cols,'s') != 0) ;
	printf("\nPatrol Boat\n");
	while(placeShip(2,gameBoard,rows,cols,'p') != 0) ;
	
	return(gameBoard);
}

FILE* openFile(int* rows, int* cols,char* buf)
{
	FILE* fp = fopen("./testBoard", "r");
	if(fp == NULL)
	{
		printf("File pointer is empty.\n");
		exit(0);
	}

	size_t sz = 50;
	int i = 0;
	if(getline(&buf,&sz,fp) == -1)
	{
		printf("No stuff in file.\n");
		exit(0);
	}

	rewind(fp);

	while(getline(&buf,&sz,fp) != -1)
	{
		// printf(">>%s",buf);
		i++;
	}
	*rows = i;
	*cols = strlen(buf);

	rewind(fp);

	return(fp);
}

char** setBoard(FILE* fp, int rows, int cols,char* buf)
{
	size_t sz = 50;

	if(fp == NULL)
	{
		printf("File pointer is empty.\n");
		exit(0);
	}
	if(getline(&buf,&sz,fp) == -1)
	{
		printf("No stuff in file.\n");
		exit(0);
	}

	rewind(fp);

	char** gameBoard = malloc( rows * sizeof(char*) );

	for(int i = 0; i < rows; i++)
	{
		gameBoard[i] = malloc(sizeof(char) * cols);
	}

	for(int q = 0; q < rows; q++)
	{
		getline(&buf,&sz,fp);
		strncpy(gameBoard[q], buf,cols-1);
		gameBoard[q][cols -1] = '\0';
	}

	fclose(fp);

	return(0);
}

void freeBoard(char* gameBoard[],int rows)
{
	for(int i = rows -1; i >= 0; i--)
	{
		free(gameBoard[i]);
	}
}


int getRandomNum(int max)
{
	srand(time(NULL));

	return(rand() % max);
}

int getRandomNum10(int max)
{
	srand(time(NULL));

	return((rand() % max) +10);
}
