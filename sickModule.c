#include <Python.h>

static PyObject* JQRSolutionError;


static PyObject* sickModule_SayHello(PyObject* self, PyObject* args)
{
	const char* var;

	if( !PyArg_ParseTuple(args,"s",&var) )
	{
		PyErr_SetString(JQRSolutionError, "No args. Expected str.\n");

		return(NULL);
	}

	printf("You've passed in: %s\n",var);
	return( Py_BuildValue("i",21) );
}


static PyMethodDef sickSolutions_Methods[] = 
{
	{"hi", sickModule_SayHello, METH_VARARGS, "Say a mighty fine hello!"}
};


static struct PyModuleDef sickModule= {
	PyModuleDef_HEAD_INIT,
	"sickModule",
	"Testing Module",
	-1,
	sickSolutions_Methods
};


PyMODINIT_FUNC PyInit_sickSolutions(void)
{
	PyObject* moduleObj = PyModule_Create(&sickModule);

	return(moduleObj);
}