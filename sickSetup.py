
from distutils.core import setup, Extension

HelloModule = Extension('sickSolutions', sources = ['sickModule.c'])

setup( name = 'Hello', 
		version='1.0',
		description='Sick module.',
		author='Sickness', 
		url='www.google.com',
		ext_modules=[HelloModule] )