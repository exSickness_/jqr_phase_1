#ifndef bsfunc_h
 #define bsfunc_h
	
int getRandomNum10(int max);
int getRandomNum(int max);
int placeShip(int len, char* gameBoard[],int rows, int cols, char ship);
int writeToFile(char* gameBoard[],int rows,int cols);
int fireShot(char* gameBoard[],int x, int y);
int countShips(char* gameBoard[],int rows, int cols);
char** makeBoard(int rows, int cols);
FILE* openFile(int* rows, int* cols,char* buf);
char** setBoard(FILE* fp, int rows, int cols,char* buf);
void freeBoard(char* gameBoard[],int rows);
int menu(int testVal);

void printBoard(char* gameBoard[],int rows, int cols);

#endif