#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>

#include "bsfunc.h"

int main(int argc, char* argv[])
// int menu(int testVal)
{
	
	printf("1.Make a board.\n2. Load board.\n0. Quit.\n");
	char userBuf[20];
	fgets(userBuf,20,stdin);

	int rows = 10, cols = 10;
	char** gameBoard;

	if(userBuf[0] == '1')
	{
		
		gameBoard = makeBoard(&rows,&cols);
	}
	else if(userBuf[0] == '2')
	{
		char* buf = malloc(sizeof(char) *50);

		FILE* fp = openFile(&rows,&cols,buf);
		gameBoard = setBoard(fp,rows,cols,buf);
		
		free(buf);
	}
	else if(userBuf[0] == '0')
	{
		return(0);
	}
	else
	{
		printf("Invalid input.\n");
	}

	printf("rows: %d\ncols: %d\n",rows,cols);

	int loop = 1;
	while(loop)
	{
		printf("1. Write board to file.\n2. Fire shot.\n3. Print Board.\n4. Count ships.\n0. Quit.\n");
		char buf[20];
		fgets(buf,20,stdin);

		if(buf[0] == '1')
		{
			writeToFile(gameBoard,rows,cols);
		}
		else if(buf[0] == '2')
		{
			printf("Enter x coord:\n");
			fgets(buf,20,stdin);
			int x = strtol(buf,NULL,10);

			printf("Enter y coord:\n");
			fgets(buf,20,stdin);
			int y = strtol(buf,NULL,10);

			fireShot(gameBoard,x,y);
		}
		else if(buf[0] == '3')
		{
			printBoard(gameBoard,rows,cols);
		}
		else if(buf[0] == '4')
		{
			int count = countShips(gameBoard,rows,cols);
			printf("Number of ships alive: %d\n",count);
		}
		else if(buf[0] == '0')
		{
			freeBoard(gameBoard,rows);
			loop = 0;
		}
		else
		{
			printf("Invalid input.\n");
		}
	}
	printf("argc is: %d\nargv[0] is: %s\n",argc,argv[0]);
	// printf("TESTVAL>>%d\n",testVal);
	return(0);
}
