CFLAGS+=-std=c11 -Wall -Werror -Wno-deprecated -Wextra -pedantic -D _XOPEN_SOURCE=800 # -Wstack-usage=1024 # -fstack-usage

all: bs_sick

bs_sick: bsfunc.o

.PHONY: clean debug

clean:
	rm bs_sick *.o *.su

debug: CFLAGS+=-g
debug: all
