from ctypes import *

sicklibrary = CDLL('/home/pewpew/Desktop/JQR_PHASE_1/sicklibrary.so')

userInput = input("1. Make a board\n2. Load board\n0. Quit\nMake your choice>>")

getRandomNum10 = sicklibrary.getRandomNum10
getRandomNum10.restype = c_int
getRandomNum10.argtypes = [c_int]

if userInput[0] == '1':
	rows = getRandomNum10(c_int(30))
	cols = getRandomNum10(c_int(30))

	print("rows: ",rows, "cols: ",cols)
	print("sigh")

	makeBoard = sicklibrary.makeBoard
	makeBoard.restype = c_void_p
	makeBoard.argtypes = [c_int, c_int]
	gameBoard = makeBoard(rows,cols)

print("bang")
while( userInput[0] != '0' ):
	userInput = input("1. Write board to file.\n2. Fire shot.\n3. Print Board.\n4. Count ships.\n0. Quit.\n\nMake your choice>>")

	if userInput[0] == '3':
		printBoard = sicklibrary.printBoard
		printBoard.restype = None
		printBoard.argtypes = [c_void_p, c_int, c_int]
		printBoard(gameBoard,rows,cols)
	elif userInput[0] == '4':
		countShips = sicklibrary.countShips
		countShips.restype = c_int
		countShips.argtypes = [c_void_p, c_int, c_int]
		count = countShips(gameBoard,rows,cols)
		print("Number of ships alive: ",count)

	elif userInput[0] == '2':
		userGuess = input("Enter coordinates to fire at>>")

		userGuess = userGuess.split(',')
		x = c_int(int(userGuess[0]))
		y = c_int(int(userGuess[1]))

		fireShot  = sicklibrary.fireShot
		fireShot.restype = c_int
		fireShot.argtypes = [c_void_p, c_int, c_int]
		retV = fireShot(gameBoard,x,y)
	elif userInput[0] == '1':
		writeToFile = sicklibrary.writeToFile
		writeToFile.restype = c_int
		writeToFile.argtypes = [c_void_p, c_int, c_int]
		retV = writeToFile(gameBoard,rows,cols)
	print("===================================================\n")

# strchr.restype = c_char_p
# >>> strchr.argtypes = [c_char_p, c_char]
# >>> strchr("abcdef", "d")
# 'def'





# a = sicklibrary.menu(15)

# int getRandomNum(int max);
# int placeShip(int len, char* gameBoard[],int rows, int cols, char ship);
# int writeToFile(char* gameBoard[],int rows,int cols);
# int fireShot(char* gameBoard[],int x, int y);
# int countShips(char* gameBoard[],int rows, int cols);
# char** makeBoard(int rows, int cols);
# FILE* openFile(int* rows, int* cols,char* buf);
# char** setBoard(FILE* fp, int rows, int cols,char* buf);
# void freeBoard(char* gameBoard[],int rows);
# int menu(int testVal);

# void printBoard(char* gameBoard[],int rows, int cols);